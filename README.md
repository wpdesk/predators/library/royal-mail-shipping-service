[![pipeline status](https://gitlab.com/wpdesk/predators/library/royal-mail-shipping-service/badges/main/pipeline.svg)](https://gitlab.com/wpdesk/royal-mail-shipping-service/pipelines) 
[![coverage report](https://gitlab.com/wpdesk/predators/library/royal-mail-shipping-service/badges/main/coverage.svg?job=unit+test+lastest+coverage)](https://gitlab.com/wpdesk/royal-mail-shipping-service/commits/main) 
[![Latest Stable Version](https://poser.pugx.org/wpdesk/royal-mail-shipping-service/v/stable)](https://packagist.org/packages/wpdesk/royal-mail-shipping-service) 
[![Total Downloads](https://poser.pugx.org/wpdesk/royal-mail-shipping-service/downloads)](https://packagist.org/packages/wpdesk/royal-mail-shipping-service) 
[![Latest Unstable Version](https://poser.pugx.org/wpdesk/royal-mail-shipping-service/v/unstable)](https://packagist.org/packages/wpdesk/royal-mail-shipping-service) 
[![License](https://poser.pugx.org/wpdesk/royal-mail-shipping-service/license)](https://packagist.org/packages/wpdesk/royal-mail-shipping-service) 

# Royal Mail Shipping Service
