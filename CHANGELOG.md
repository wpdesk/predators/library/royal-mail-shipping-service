## [1.3.2] - 2024-10-23
### Fixed
- settings default values

## [1.3.1] - 2023-05-18
### Fixed
- debug messages

## [1.3.0] - 2023-05-17
### Added
- multi package shipments

## [1.2.2] - 2023-02-14
### Fixed
- compensation consideration

## [1.2.1] - 2022-09-26
### Fixed
- default values for weight and dimensions

## [1.2.0] - 2022-08-16
### Added
- en_PL, en_GB translators

## [1.1.0] - 2022-07-04
### Added
- taxes in price lists

## [1.0.3] - 2022-07-04
### Fixed
- texts

## [1.0.1] - 2022-06-29
### Fixed
- settings

## [1.0.0] - 2022-06-27
### Added
- initial version
